```bash
#!/bin/bash

#SBATCH -A edu20.mmhpc
#SBATCH --nodes 1
#SBATCH --time 1:00:00
#SBATCH --job-name jupyter
#SBATCH --output jupyter-log-%J.txt

module load anaconda/py36/5.0.1
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/pdc/vol/anaconda/co7/5.0.1/py36_dep/openblas/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lib64
export OMP_NUM_THREADS=24

## set an env variable, find hostname of compute node
XDG_RUNTIME_DIR=""
NODENAME=$(hostname)

## print tunneling instructions to jupyter-log-{jobid}.txt
echo -e "
    Copy/Paste this in your local terminal to ssh tunnel with remote
    -----------------------------------------------------------------
    ssh -N -L 8888:localhost:8888 $USER@$NODENAME
    -----------------------------------------------------------------

    Then open a browser on your local machine to the following address
    ------------------------------------------------------------------
    https://localhost:8888
    ------------------------------------------------------------------
    "

## launch jupyter server
jupyter-notebook --no-browser
```