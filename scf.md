```python
from mpi4py import MPI
import veloxchem as vlx
import sys

# geometry in Angstrom
molecule = vlx.Molecule.read_str("""
    O 0 0 0
    H 0 0 1.795239827225189
    H 1.693194615993441 0 -0.599043184453037""")

basis = vlx.MolecularBasis.read(molecule, '6-31G')

comm = MPI.COMM_WORLD
ostream = vlx.OutputStream(sys.stdout)
ostream.print_block(molecule.get_string())
ostream.print_block(basis.get_string('Atomic Basis', molecule))

scfdrv = vlx.ScfRestrictedDriver(comm, ostream)
scfdrv.compute(molecule, basis)
```