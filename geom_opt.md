```python
from mpi4py import MPI
import veloxchem as vlx

# geometry in Angstrom
molecule = vlx.Molecule.read_str("""
    O 0 0 0
    H 0 0 1.795239827225189
    H 1.693194615993441 0 -0.599043184453037""")

basis = vlx.MolecularBasis.read(molecule, '6-31G')

comm = MPI.COMM_WORLD
ostream = vlx.OutputStream()

optdrv = vlx.OptimizationDriver(comm, ostream)
optdrv.update_settings({'coordsys': 'tric'}, {}, {'xcfun': 'b3lyp'})
final_mol = optdrv.compute(molecule, basis)

print('\nFinal geometry:\n')
for a, xyz in zip(final_mol.get_labels(), final_mol.get_coordinates()):
    print('{} {} {} {}'.format(a, *xyz))
```