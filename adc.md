```python
import gator

# geometry in Angstrom
molecule = gator.get_molecule("""
    O 0 0 0
    H 0 0 1.795239827225189
    H 1.693194615993441 0 -0.599043184453037""")

basis = gator.get_molecular_basis(molecule, '6-31G')

scf = gator.run_scf(molecule, basis, conv_thresh=1e-8)
adc = gator.run_adc(molecule, basis, scf, method='adc2', singlets=5)
```