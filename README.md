# Molecular Modeling on HPC Systems

## Login to PDC's machines

+ [How to login from Linux](https://www.pdc.kth.se/support/documents/login/linux_login.html)

+ [How to login from Windows](https://www.pdc.kth.se/support/documents/login/windows_login.html)

+ [How to login from MacOS](https://www.pdc.kth.se/support/documents/login/mac_login.html)

## Run jobs in Jupyter notebook

+ [General instruction on how to use Jupyter notebooks](https://www.pdc.kth.se/software/software/Jupyter-Notebooks/centos7/5.0.0/index_using.html)

+ [Submission script for running Jupyter notebook on a compute node of Tegner](jupyter.md)

+ [Running SCF calculation in Jupyter notebook](scf.md)

+ [Running geometry optimization in Jupyter notebook](geom_opt.md)

+ [Running MP2 calculation in Jupyter notebook](mp2.md)

+ [Running ADC calculation in Jupyter notebook](adc.md)

## Submit jobs via batch script

+ [Submission script for running GATOR on Tegner](sbatch_gator_tegner.md)

+ [Input file for MP2 calculation](mp2_input.md)

## Files for the exercises

+ [Experimental C K-edge XAS spectrum of vinylfluoride](vinylfluoride.dat)
